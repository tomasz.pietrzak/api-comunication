import { registerWithEmailAndPassword, auth } from '../firebase';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useNavigate } from 'react-router-dom'
import { useEffect, useState } from 'react';

const Signup = () => {
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null)
    
    const [user, loading, error] = useAuthState(auth);
    const navigate = useNavigate();
    const handleSignup = () => {
        registerWithEmailAndPassword(email, password);
    }

    useEffect(() => {
        if(loading) { return };
        if(user) { navigate('/login') };
    },[user, loading, navigate]);

    useEffect(() => {
        console.log(error);
    },[error]);

    return(
        <>
            <h1>Signup</h1>
            <h3>Signup page</h3>
            <label htmlFor='email'>Email</label>
            <input id='email' type='email' name='email' placeholder='Email' onChange={(e) => setEmail(e.target.value)}/>
            <label htmlFor='password'>Password</label>
            <input id='password' type='password' name='password' placeholder='Placeholder' onChange={(e) => setPassword(e.target.value)}/>
            <button onClick={() => handleSignup()}>Sign Up</button>
        </>
    )
}

export default Signup