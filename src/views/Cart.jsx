import { CartContext } from "../CartContext";
import { useContext, useEffect, useState } from "react";
import { auth } from "../firebase";
import { useAuthState } from "react-firebase-hooks/auth";
import { addOrder } from '../store/api';

const Cart = () => {
    const { order, setOrder } = useContext(CartContext);
    const [cart, setCart] = useState([]);
    const [user] = useAuthState(auth);

    useEffect(() => {
        if(order) {
            setCart(Object.keys(order));
        }
    },[order, order?.length])

    const removeItem = (id) => {
        delete order[id];
        setOrder(order);
        setCart(Object.keys(order));
    }

    if(!order) {
        return (<>
            <div>
                <h5>Hey Bee!</h5>
                <span>Your cart is empty :(</span>
            </div>
        </>)
    }

    return (
        <div className='container'>
            {cart.map(i => {
                return (
                    <li key={i}>Name: {order[i].name}, Amount: {order[i].amount} Price for all: {order[i].value}<button className='button' onClick={() => removeItem(i)}>delete</button></li>
                )
            })}
            <span className='footer'>
                <button onClick={() => setOrder(null)}>Cleare All</button>
                <button className='button' onClick={() => {addOrder(order, user.uid, user.email)}}>Send Order</button>
            </span>
        </div>
    )
}

export default Cart;