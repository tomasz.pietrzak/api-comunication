import { logInWithEmailAndPassword, auth, logout } from "../firebase";
import { useAuthState } from "react-firebase-hooks/auth";
import { useEffect, useState } from "react";

const Login = () => {
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null)
    const [user, loading, error] = useAuthState(auth);

    const handleLogin = () => {
        logInWithEmailAndPassword(email, password);
    }

    useEffect(() => {
        if(loading) {return}
    }, [loading, user])

    useEffect(() => {
        console.log(error);
    },[error]);

    return(
        <>
            <h1>Login</h1>
            <h3>Login page</h3>
            {!user ? 
                <>
                    <label htmlFor='email'>Email</label>
                    <input id='email' type='email' name='email' placeholder='Email' onChange={(e) => setEmail(e.target.value)}/>
                    <label htmlFor='password'>Password</label>
                    <input id='password' type='password' name='password' placeholder='Placeholder' onChange={(e) => setPassword(e.target.value)}/>
                    <button onClick={() => handleLogin()}>Log In</button>
                </>
            : 
                <div>
                    <span>You are successfuly loged in as:</span>
                    <span>{user?.email}</span>
                    <button onClick={() => logout()}>Log Out</button>
                </div>
            }
        </>
    )
}

export default Login;