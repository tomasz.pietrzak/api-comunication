import { useParams } from "react-router";
import { useEffect, useState, useContext } from "react";
import { CartContext } from "../CartContext";
import { Link } from "react-router-dom";
import { getItemFromApi } from '../store/api';

const Item = () => {
    const { id } = useParams();
    const [item, setItem] = useState(null);
    const {setOrder, order } = useContext(CartContext);
    const [amount, setAmount ] = useState(0)

    useEffect(() => {
        getItemFromApi(id, setItem);
    },[id])

    const addToCart = (name = 'unknown', price = 0) => {
        setOrder({...order, [id]: {name, amount: amount, value: price*amount}});
    }

    if(item === null) {
        return <span>Loading...</span>
    }

    return(
        <>
            <Link to='/cart'>Cart</Link>
            {typeof item === 'string' ?
            <span>{item}</span>
            :
            <div className='item-container'>
                <h3>{item.name}</h3>
                <span><strong>About:</strong> {item.description}</span>
                <span><strong>Available:</strong> {item.amount}</span>
                <span><strong>Price:</strong> {item.price}</span>
                <span className='item-footer'>
                    <input type='number' value={amount} onChange={e => setAmount(e.target.value)}/>
                    <button className='button' onClick={() => addToCart(item.name, item.price)}>Add to cart</button>
                </span>
            </div>
            }
        </>
    )
}

export default Item;