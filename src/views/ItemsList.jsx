import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { getItemsFromApi } from '../store/api';

const ItemsList = () => {
    const [items, setItems] = useState(null);

    useEffect(() => {
        getItemsFromApi(setItems)
    }, []);
    
    return (
        <>
            <h3>Items list:</h3>
            <ul>
                <>
                    {items !== null ?
                    items.map(item => <li key={item.id}><Link to={`/item/${item.id}`}>Name: {item.data().name}</Link> Amount: {item.data().amount} Price: {item.data().price}</li>)
                    :
                    <span>There are no items on list!</span>}
                </>
            </ul>
        </>
    ) 
}

export default ItemsList