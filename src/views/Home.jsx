import { Link } from 'react-router-dom';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from '../firebase';
const Home = () => {
    const [user] = useAuthState(auth);
    return(
        <nav className='nav'>
            <Link to='/login' className='nav-button'>Login</Link>
            <Link to='/signup' className='nav-button'>Signup</Link>
            {user ?
                <>
                    <Link to='/items-list' className='nav-button'>Items</Link>
                    <Link to='/cart' className='nav-button'>Cart</Link>
                </>
                :
                null
            }
        </nav>
    )
}

export default Home;