import axios from "axios";
import { doc, getDoc, collection, addDoc, query, getDocs } from "firebase/firestore";
import { db } from "../firebase";

export const getItemFromApi = async (id, setItem) => {
    const docRef = doc(db, 'products', id);
    const docSnap = await getDoc(docRef);
    if (docSnap.exists()) {
        setItem(docSnap.data());
    } else {
        setItem('No item was found!')
    }
}

export const addOrder = async (order, uid, email) => {
    const docRef = await addDoc(collection(db, "orders"), {...order, uid, email});
    return console.log(docRef.id);
}

export const getItemsFromApi = async (setItems) => {
    const q = query(collection(db, 'products'));
    const snapshot = await getDocs(q);
    const _tmp = [];
    snapshot.forEach(doc => {
        _tmp.push(doc);
    })
    setItems(_tmp);
}

export const getRandomPhotoAxios = async () => {
    const response = await axios.get('https://dog.ceo/api/breeds/image/random');
    console.log('Axios response ', response);
    const json = response.data;
    console.log('Axios data ', json)
    return json;
}

/*
    Fetch documentation:
    https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
*/

export const getRandomPhotoFetch = async () => {
    const response = await fetch('https://dog.ceo/api/breeds/image/random', {
        method: 'GET',
    });
    console.log('Fetch respone ',response);
    const json = await response.json();
    console.log('Fetch data ', json)
    return json;
}