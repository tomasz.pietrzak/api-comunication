import './App.css';
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import Home from './views/Home';
import Login from './views/Login';
import Signup from './views/Signup';
import ItemsList from './views/ItemsList';
import Item from './views/Item';
import Cart from './views/Cart';
import { CartContextProvider } from './CartContext';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from './firebase';
import { getRandomPhotoAxios, getRandomPhotoFetch } from './store/api';
import { useEffect, useState } from 'react';

function App() {
  const [user] = useAuthState(auth);
  const [axiosImg, setAxiosImg] = useState(null);
  const [fetchImg, setFetchImg] = useState(null);

  const getData = async () => {
    const axiosDog = await getRandomPhotoAxios();
    const fetchDog = await getRandomPhotoFetch();

    setAxiosImg(axiosDog);
    setFetchImg(fetchDog);
  }

  useEffect(() => {
    if(axiosImg === null || fetchImg === null) {
      getData()
    }    
  },[axiosImg, fetchImg])

  return (
    <CartContextProvider>
      <h1 className='title'>HiveShop</h1>
      <h4 className='title'>best place for your dog</h4>
      <div className='dogs'>
        <div className='quote-container'>
          <img className='dog' src={axiosImg?.message} alt='dog from axios'/>
          <div className='quote'>
            <span className='quote-text'>"Woof! The beest dog shop ever"</span>
            <span className='quote-dog'>Burek</span>
          </div>
        </div>
        <div className='quote-container'>
          <img className='dog' src={fetchImg?.message} alt='dog from fetch'/>
          <div className='quote'>
            <span className='quote-text'>"Thanks to these products, I can't stop wagging my tail."</span>
            <span className='quote-dog'>Azor</span>
          </div>
        </div>
      </div>
      <BrowserRouter>
        <Home/>
        <Routes>
          <Route path='/' element={<>Let`s start Bees...</>} />
          <Route path='/login' element={<Login/>}/>
          <Route path='/signup' element={<Signup/>}/>
          {user ? 
            <>
              <Route path='/items-list' element={<ItemsList/>}/>
              <Route path='/item/:id' element={<Item/>}/>
              <Route path='/cart' element={<Cart/>}/>
            </>
          :
            <>
              <Route path='*' element={<Login/>}/>
            </>
          }
          
        </Routes>
      </BrowserRouter>
    </CartContextProvider>
  );
}

export default App;
