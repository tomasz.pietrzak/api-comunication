import { createContext, useState } from "react";

export const CartContext = createContext();

export const CartContextProvider = ({ children }) => {
    const [order, setOrder] = useState(null);

    return (
        <CartContext.Provider value = {{ order, setOrder }}>
            { children }
        </CartContext.Provider>
    )
}